import java.io.IOException;
import java.io.StringReader;
import org.xml.sax.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;
/**
 * Created by Grace on 9/30/2015.
 */
public class ReadingXML extends DefaultHandler {
    public static void main(String[] args) throws Exception {
        String xmlContents = "<Students>\n" +
                "<Student Name=\"April\" Gender=\"F\" DateOfBirth=\"1989-01-02\" />\n" +
                "<Student Name=\"Bob\" Gender=\"M\"  DateOfBirth=\"1990-03-04\" />\n" +
                "<Student Name=\"Chad\" Gender=\"M\"  DateOfBirth=\"1991-05-06\" />\n" +
                "<Student Name=\"Dave\" Gender=\"M\"  DateOfBirth=\"1992-07-08\">\n" +
                "  <Pet Type=\"dog\" Name=\"Rover\" />\n" +
                "</Student>\n" +
                "<Student DateOfBirth=\"1993-09-10\" Gender=\"F\" Name=\"&#x00C9;mily\" />\n" +
                "</Students>";
        ReadingXML info = new ReadingXML();
        info.parse(new InputSource(new StringReader(xmlContents)));
    }
    public void startElement(String uri, String localName, String qName, Attributes attri) throws SAXException {
        if (qName.equals("Student")) {
            System.out.println(attri.getValue("Name"));
        }
    }
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
    }
    //Parse an input source to the XMLReader
    public void parse(InputSource source) throws SAXException, IOException {
        XMLReader parser = XMLReaderFactory.createXMLReader();
        parser.setContentHandler(this);
        parser.parse(source);
    }
    public void characters(char[] chr, int start, int length) throws SAXException {
        super.characters(chr, start, length);
    }
}
