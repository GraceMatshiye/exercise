/**
 * Created by Grace on 10/8/2015.
 * This Class implements the wc command in java
 */
import java.io.*;
public class UnixWcCommand {
    public static void main(String[] args) {
        System.out.println("Counting the lines, words and characters in a UnixTextFile.txt file");
        String fileName = "C:\\Users\\grace\\Documents\\DigitalGeekaship\\UnixTextFile.txt";
        BufferedReader reader = null;
        try{
            FileReader fileReader = new FileReader(fileName);
            reader = new BufferedReader(fileReader) ;
            count(fileName,reader);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }finally {
            if(reader != null){
                try{
                    reader.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }
    //This method reads every line in the text file
    public static void count(String fileName, BufferedReader reader) throws IOException {
        String line = "";
        String[] words;
        int numberOfWord = 0;
        int lines = 0;
        int allChars = 0;
        while((line = reader.readLine()) != null){
            lines++;
            allChars += line.length();
            words = line.split(" ");
            numberOfWord += countWord(line);
        }
        System.out.println("Total Lines :"+ lines);
        System.out.println("Total Characters :"+ allChars);
        System.out.println("Total words :"+ numberOfWord );
    }
    ///This method count the number of words
    public static long countWord(String sentence){
        int number = 0;
        int index = 0;
        boolean preWhiteSpace = true;
        while(index < sentence.length()){
            char aChar = sentence.charAt(index++);
            boolean currentWhiteSpace = Character.isWhitespace(aChar);
            if(preWhiteSpace && !currentWhiteSpace){
                number++;
            }
            preWhiteSpace = currentWhiteSpace;
        }
        return number;
    }
}
