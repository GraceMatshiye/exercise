import java.nio.file.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by Grace on 9/22/2015.
 * C:\Users\grace\Documents
 * C:\Users\grace\Documents\DigitalGeekaship
 */
public class AddFromFile {

    public static void main(String[] args){

        String path = "C:\\Users\\grace\\Documents\\DigitalGeekaship\\Number.txt";

        try{
            FileReader file = new FileReader(path);

            getListItems(file);

        }
        catch (IOException e)
        {
            System.out.println("The file doesn't exist");
        }
    }

  //A method that read from a file
   public static void  getListItems(FileReader file)throws IOException {

        BufferedReader readFile = new BufferedReader(file);
        String stringCurrentLine  = readFile.readLine();
        int total = 0;

        ArrayList<String> list = new ArrayList<>();

        while (stringCurrentLine != null) {

            total += Integer.parseInt(stringCurrentLine);
            list.add(stringCurrentLine);
            stringCurrentLine = readFile.readLine();
        }
        readFile.close();
       display(list,total);

    }

    //A method that display output
    public static void display(ArrayList<String> list, int total)throws IOException {
        String message = "";

        for (int index = 0; index < list.size(); index++) {

            if (index < list.size() - 1) {
                message += list.get(index) + " + ";
            } else {
                message += list.get(index) + " = ";
            }
        }
        System.out.println("\n" + message + total);

    }
}


