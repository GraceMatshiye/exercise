/**
 * Created by Grace on 9/17/2015.
 */

import javax.swing.JOptionPane;

public class SumOfDigits {

    public static void main(String[] args){

        String digit = JOptionPane.showInputDialog(null, "Enter more than five digits");

        int length = digit.length();
        char[] numbers = digit.toCharArray();
        int sum = 0;


        for(int count = 0 ; count < length ; count++){
            sum = sum +  Character.getNumericValue(numbers[count]) ;

        }

        JOptionPane.showMessageDialog(null, "The sum for " + digit + " is " + sum);
    }
}