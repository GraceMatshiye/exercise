import javax.swing.*;

/**
 * Created by Grace on 9/21/2015.
 */
public class EvenOrOddNumber {

    public static void main( String[] args){

        int number = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter any number to check if its Even or Odd"));

        int strNumber = Integer.parseInt(JOptionPane.showInputDialog(null, "There is more than one way to solve this task \n" +
                "Press 1 if you want to use modulo, Press 2 for Bitwise."));

        int answer = number % 2;

        if(strNumber == 1){

            getTheAnswer(modulo(number), number);
        }
        else if(strNumber == 2){
            getTheAnswer(bitwise(number), number);
        }


    }

    //method to determine an even or odd number using Bitwise
    public static boolean bitwise(int number){

        return((number & 1) == 0);

    }

    //method to determine an even or odd number using modulo
    public static boolean modulo(int number){
        return((number % 2) == 0);
    }

    public static void getTheAnswer(boolean isEvenOdd, int number){

        String message = "The number you entered " + number + " is ";

        if(isEvenOdd == true ){
            JOptionPane.showMessageDialog(null, message + " Even.");
        }
        else{
            JOptionPane.showMessageDialog(null, message + " Odd.");
        }

    }
}