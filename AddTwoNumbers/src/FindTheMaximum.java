/**
 * Created by Grace on 9/21/2015.
 */
public class FindTheMaximum {
     public static void main(String[] args){

         int[] seriesOfNumbers = {2,34,56,678,56,3,567,354,656,9,2,7,1};
         int maximun = seriesOfNumbers[0];
         String allNumber = "All numbers in the array:\n";

         for(int count = 0; count < seriesOfNumbers.length; count++){


             if(maximun < seriesOfNumbers[count]) {

                 maximun = seriesOfNumbers[count];

             }
             allNumber = allNumber + seriesOfNumbers[count] + "\n";
         }

         System.out.println(allNumber + "The Maximum : " + maximun);
     }
}
