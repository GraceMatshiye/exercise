
/**
 * Created by Grace on 9/17/2015.
 */

import javax.swing.JOptionPane;

public class AddTwoNumber {

    public static void main (String[] args){

        String input = JOptionPane.showInputDialog(null, "Enter two numbers separated by a space" );


        String[] numbers = input.split(" ");

        int sum = Integer.parseInt(numbers[0]) + Integer.parseInt(numbers[1]);

        JOptionPane.showMessageDialog(null, input + "\nThe sum of two numbers you entered = " + sum);


    }
}