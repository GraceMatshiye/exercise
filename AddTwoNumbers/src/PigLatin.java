import java.util.Scanner;

/**
 * Created by Grace on 9/25/2015.
 */
public class PigLatin {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a String and get the pig-latin version");
        String word = input.nextLine();
        char firstLetter = Character.toLowerCase(word.charAt(0));
        int count = 0;

        //testing if the first letter is a vowel
        if(isVowel(firstLetter) == true ){
            vowelSyllable(word);

        }
        //A method that test if the first and second letter are consonant
        else {

            for(int c = 0; c< word.length() ; c++) {
                char secondLetter = Character.toLowerCase(word.charAt(c));
                if(isVowel(secondLetter) != true )
                    count++;
                else
                    c = word.length();
            }
            consonantsSyllable(word,count);

        }

        System.out.println("Please enter the pig-latin word ");
        word = input.nextLine();
        reverseLatin(word);


    }
    //A method that test if the letter is a vowel
   public static boolean isVowel(char letter){
       boolean results;
      if(letter == 'a' || letter == 'e'  || letter == 'i' || letter == 'o'  || letter == 'u' )
               results =true ;
           else
               results = false ;

       return results ;
    }

    //A method that convert english word to a pig-latin
    public static void vowelSyllable(String word){

        String syllable = "-way";

        System.out.println("The pig-latin version: " + word + syllable);

    }

    //A method that convert english word to a pig-latin
    public static void consonantsSyllable(String word, int number){

        char hyphen = '-';
        String syllable = "ay";
        String consonant = "";
            for(int x = 0; x< number ;x++){
                 consonant = consonant + word.charAt(x);
            }

            word = word.substring(number);
            System.out.println("The pig-latin version: " + word + hyphen + consonant + syllable);


    }

    //A method that convert pig-latin word to an english word
    public static void reverseLatin(String word){

        String[] syllable = word.split("-");
        String consonant = "";
        String message ="From pig-latin to english word : " + word + "  >>>>  " ;

        if(syllable[1].equalsIgnoreCase("way")) {
            System.out.println( message + syllable[0]);
        }else{

            for (int x = 0; x < syllable[1].length(); x++) {
                if (!isVowel(syllable[1].charAt(x))) {
                    consonant = consonant + syllable[1].charAt(x);
                } else
                    x = syllable[1].length();
            }

            message += consonant + syllable[0];


            System.out.println(message);
        }
    }
}
