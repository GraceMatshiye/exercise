import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Grace on 10/9/2015.
 */
public class RockPaperScissors {
    public static void main(String args[]){

        int player1 = 0, player2 = 0, win = 0;
        ArrayList<Integer> arrList = new ArrayList<Integer>();
        boolean endGame = false;
        int[] record = null;
        String message = "";
        int numberOfGamesPlayed = 5;
        while(!endGame) {
            String input = JOptionPane.showInputDialog(null, "Enter one of the following number from 1 to 3 " +
                    "\nto choose one of rock, paper or scissors" +
                    "\n1 - Rock \n2 - Paper \n3 - Scissors" +
                    "\n9 - if you want to end this game.");
           if(isValid(input)){
               if(Integer.parseInt(input) != 9) {
                   numberOfGamesPlayed--;
                   player2 = Integer.parseInt(input);
                   arrList.add(player2);
                   record = checkFrequency(arrList);
                   if(numberOfGamesPlayed <=0){
                       player1 = getFrequentNumber(record);
                       if(player1 == 1){
                           player1 = 2;
                       }else if(player1 == 2){
                           player1 = 3;
                       }else {
                           player1 = 1;
                       }
                   }else{
                       player1 = computerOpponent();
                   }
                   //Determines the winner
                   win = winner(player1, player2);
                   if(win == 0){
                       message = toString(win);
                   }else if(win == player1){
                       message = "Player1 won!!";
                   }else if(win == player2) {
                       message = "Player2 won!!";
                   }
                   System.out.println("Player 1 (Computer): " + toString(player1) + "\t\t" +
                           "Player 2 (You): " + toString(player2) + "\n" + message);
                   endGame = false;
               }else{
                   endGame =true;
               }
           }else{
              endGame = false;
           }

        }
        System.out.println(arrList);
    }
    //A method that validate the User's input
    public static boolean isValid(String input){
        if(Character.isDigit(input.charAt(0)) && input.length() == 1 ){
            int number = Integer.parseInt(input);
            if(number == 1 || number == 2 || number == 3 || number == 9 ){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    //A method that generate random number between 1 and 3
    public static int computerOpponent(){
        int answer = 0;
        Random rNumber = new Random();
        while(answer == 0) {
            answer = rNumber.nextInt(4);
        }
        return answer;
    }
    public static String toString(int number){
        String answer = "";
        switch(number){
            case 1 :
                answer = "Rock";
            break;
            case 2:
                answer = "Paper";
            break;
            case 3:
                answer = "Scissors";
            break;
            case 0:
                answer = "No winner";
                break;
        }
        return answer;
    }
    //A method that determine the winner between the Player1 and Player2
    public static int winner(int opponent1, int opponent2){
        if(opponent1 == 1 && opponent2 ==3){
            return opponent1;
        }else if(opponent1 == 3 && opponent2 ==2){
            return opponent1;
        }else if(opponent1 == 2 && opponent2 ==1){
            return opponent1;
        }else if(opponent2 == 1 && opponent1 ==3){
            return opponent2;
        }else if(opponent2 == 3 && opponent1 ==2){
            return opponent2;
        }else if(opponent2 == 2 && opponent1 ==1) {
            return opponent2;
        }else{
            return 0;
        }

    }
    //A method that record the user's input
    public static int[] checkFrequency(ArrayList<Integer> arrList){
        int one =0, two = 0, three =0;
        int[] record = new int[3];
        for(int x=0 ; x< arrList.size() ;x++){
            switch(arrList.get(x)){
                case 1 :
                    one++;
                    break;
                case 2:
                    two++;
                    break;
                case 3:
                    three++;
                    break;
            }
        }
        record[0] = one;
        record[1] = two;
        record[2] = three;
        return record;
    }
    public static int getFrequentNumber(int[] list){
        int max = list[0];
        for (int x = 1; x < list.length; x++) {
            if(list[x] > max ){
                max = list[x];
            }
        }
        return max;
    }
}
