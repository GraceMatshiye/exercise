import java.io.File;

/**
 * Created by Grace on 9/23/2015.
 */
public class DeleteAFile {

    public static void main(String[] args){

        String fileName = "C:\\Users\\grace\\Documents\\DigitalGeekaship\\input.txt";
        String directory = "C:\\Users\\grace\\Documents\\DigitalGeekaship\\docs";

        if(existFileOrDirectory(fileName) == true){
            if(delete(fileName) == true ){
                System.out.println("The input.txt file is deleted.");
            }
        }else {
            System.out.println("The input.txt file could not deleted.");
        }

        if(existFileOrDirectory(directory) == true){
            if(delete(directory) == true ){
                System.out.println("The docs directory is deleted.");
            }
        }else {
            System.out.println("The docs directory could not deleted.");
        }


    }

    public static boolean existFileOrDirectory(String file){

        boolean exist = new File(file).isDirectory();

        return exist;
    }

    public static boolean delete(String file){

        boolean deleted = new File(file).delete();

        return deleted;
    }
}
