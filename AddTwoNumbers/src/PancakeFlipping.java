import com.sun.org.apache.xml.internal.security.algorithms.JCEMapper;

/**
 * Created by Grace on 10/8/2015.
 */
public class PancakeFlipping {
    int[] array;
    public static void main(String[] args) {
        int[] numbers = {1, 2, 5, 4, 3, 10, 9, 8, 7};
        System.out.println("Pancake Sort Algorithm");
         for (int i = 0; i < numbers.length; ++i)
             System.out.print(numbers[i] + "\t");
        PancakeFlipping pan = new PancakeFlipping(numbers);
        System.out.println("\n" + pan.toString());
    }
    public String toString() {
        String message = "";
        for (int x: array)
            message += x + " ";
        return message;
    }
    public void flip(int n) {
        for (int i = 0; i < (n+1) / 2; ++i) {
            int swap = array[i];
            array[i] = array[n-i];
            array[n-i] = swap;
        }
        System.out.print("\n" + "flip(0.." + n + "): " + toString());
    }
    public int[] findMinMax(int n) {
        int min, max;
        min = max = array[0];
        int start = 0, end = 0;
        for (int i = 1; i < n; ++i) {
            if (array[i] < min) {
                min = array[i];
                start = i;
            }
            else if (array[i] > max) {
                max = array[i];
                end = i;
            }
        }
        return new int[] {start, end};
    }
    public void sort(int number, int dir) {
        if (number == 0) return;
        int[] panArray = findMinMax(number);
        int bestXPos = panArray[dir];
        int altXPos = panArray[1-dir];
        boolean flipped = false;
        if (bestXPos == number-1) {
            --number;
        }
        else if (bestXPos == 0) {
            flip(number-1);
            --number;
        }
        else if (altXPos == number-1) {
            dir = 1-dir;
            --number;
            flipped = true;
        }
        else {
            flip(bestXPos);
        }
        sort(number, dir);

        if (flipped) {
            flip(number);
        }
    }
    PancakeFlipping(int[] numbers) {
        array = numbers;
        sort(numbers.length, 1);
    }
}
