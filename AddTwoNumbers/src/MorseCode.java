import java.util.Scanner;
/**
 * Created by Grace on 10/1/2015.
 */
public class MorseCode {
    public static void main(String[] args){
        String[] letter = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
                "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8",
                "9", "0", " " };
        String[] code = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.\n",
                "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.\n",
                "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-\n",
                "-.--", "--..", ".----", "..---", "...--", "....-", ".....\n",
                "-....", "--...", "---..", "----.", "-----", " " };
        System.out.println("Letter and digits");
        for(String s: letter){
            System.out.print(s + "  ");
        }
        System.out.println("\nMorse Code");
        for(String s: code){
            System.out.print("\t" + s);
        }
        Scanner input = new Scanner(System.in);
        System.out.println("\n\nEnter any english word or/and numbers to convert from English or Morse to convert from Morse:\n" +
                "When entering morse code separate each code with a space");
        String answer = input.nextLine();
        if(isEnglishWord(answer) == true ){
            String tran = translateEnglish(letter, answer);
            System.out.print("\nEnglish word : \"" + answer + "\" the morse code : ");
            for(int x = 0; x < tran.length() ; x++){
                System.out.print(code[Integer.parseInt(tran.charAt(x)+"")]);
            }
        }else{
            String tran = translateMorse(code, answer);
            System.out.print("Morse code : \"" + answer + "\" the English word : ");
            for(int x = 0; x < tran.length() ; x++){
                System.out.print(letter[Integer.parseInt(tran.charAt(x)+"")]);
            }
        }
    }
    /**
     * A method that find the index of a Morse code
     * Returns String number
     * */
    public static String translateMorse(String[] code, String answer){
        String number = "";
        String[] arrAnswer = answer.split(" ", 0);
        for(int x = 0; x < arrAnswer.length ; x++) {
            for (int z = 0; z < code.length; z++) {
                if(arrAnswer[x].equalsIgnoreCase(code[z])){
                    number+= z;
                }
            }
        }
        return number;
    }
    /**
     * A method that find the index of an English word
     * Returns String number
     * */
    public static String translateEnglish(String[] letter, String answer){
        String number = "";
        char[] arrAnswer = answer.toLowerCase().toCharArray();
        for(int x = 0; x < arrAnswer.length ; x++){
            for(int z = 0; z < letter.length ; z++){
                if(arrAnswer[x] == letter[z].charAt(0)){
                    number += z;
                }
            }
        }
        return number;
    }
    /**
     * A method that determine the user's input is English or morse code
     * False means the user entered morse code
     * True means the user entered an english word od digit
     */
    public static boolean isEnglishWord(String letters){
        boolean isLetters = false;
        if(Character.isLetterOrDigit(letters.charAt(0))){
            isLetters = true ;
        }
        return isLetters;
    }
}
