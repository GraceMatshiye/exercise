/**
 * Created by Grace on 10/7/2015.
 */
public class QuickSort{
    private int array[];
    private int length;
    public static void main(String[] args) {
        QuickSort sort = new QuickSort();
        int[] input = {24, 2, 45, 20, 56, 75, 2, 56, 99, 53, 12, 33};
        sort.setSort(input);
        for (int i : input) {
            System.out.print(i);
            System.out.print(" ");
        }
    }
    public void setSort(int[] inputArr) {
        if (inputArr == null || inputArr.length == 0) {
            return;
        }
        this.array = inputArr;
        length = inputArr.length;
        quickSortFunction(0, length - 1);
    }
    private void quickSortFunction(int lowerIndex, int higherIndex) {
        int start = lowerIndex;
        int end = higherIndex;
        int pivot = array[lowerIndex + (higherIndex - lowerIndex) / 2];
        while (start <= end) {
            while (array[start] < pivot) {
                start++;
            }
            while (array[end] > pivot) {
                end--;
            }
            if (start <= end) {
                exchangeNumbers(start, end);
                start++;
                end--;
            }
        }
        // call quickSort() method recursively
        if (lowerIndex < end)
            quickSortFunction(lowerIndex, end);
        if (start < higherIndex)
            quickSortFunction(start, higherIndex);
        }
    private void exchangeNumbers(int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}