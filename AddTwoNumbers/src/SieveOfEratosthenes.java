import javax.swing.*;

/**
 * Created by Grace on 10/4/2015.
 */
public class SieveOfEratosthenes {
    public static void main(String[] args){
        int nNumber = Integer.parseInt(JOptionPane.showInputDialog(null,
                "Enter any number and will returns a list of prime numbers less than or" +
                        "\n equal to n using the optimized sieving algorithm"));
        System.out.println("The prime Number for " + nNumber );
        boolean[] primeNumbers = isPrimeNumber(nNumber);
        //Prints the prime numbers
        for(int x = 2; x < nNumber; x++){
            if(primeNumbers[x] == true){
                System.out.print(x + ", ");
            }
        }
        int example = 15485863;
        boolean[] numberOfPrime = isPrimeNumber(example);
        int count = 0;
        for(int x = 2; x < example; x++){
            if(numberOfPrime[x] == true){
                count++;
            }
        }
        System.out.println("\nThe number of primes for 15485863 : " + count);
    }
    //A method that determine the prime
    public static boolean[] isPrimeNumber(int max){
        boolean[] primeNumber = new boolean[max];
        for(int x = 2; x < max; x++){
            primeNumber[x] = true;
        }
        for(int x = 2; x < max; x++){

            if(primeNumber[x] == true ) {
                for (int y = x + x; y < max; y= y+x) {
                    primeNumber[y] = false;
                }
            }
        }
        return primeNumber;
    }
}
