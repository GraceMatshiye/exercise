import javax.swing.*;

/**
 * Created by Grace on 9/21/2015.
 */
public class ReverseIt {

    public static void main(String[] args){

        String input = JOptionPane.showInputDialog(null, "Enter a string or a number and will reverse a what you entered");

        int choice = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter 1 to reverse a String or, \nenter 2 to reverse an Array"));

        if(choice == 1){
            reverseAString(input);
        }
        else{
            reverseAnArray(input);
        }

    }

    //A method that reverse a string
    public static void reverseAString(String word){

        String reverse = "";
        for(int x = word.length()-1; x >=0 ;--x){
            reverse += word.charAt(x);
        }

        JOptionPane.showMessageDialog(null,"The word you entered : " + word  + "\nIn reverse order : " + reverse);


    }

    //A method that reverse a string using array
    public static void reverseAnArray(String word){

        int length = word.length();
        char[] stringArray = word.toCharArray();
        char[] reverseWord = new char[stringArray.length];
        String message = "The word you entered : " + word  + "\nIn reverse order : ";

        for(int index = 0 ; index < stringArray.length ; index++){

            --length;
            reverseWord[length] = stringArray[index];

        }


        for(int count = 0 ; count < reverseWord.length ; count++ ){
            message = message + String.valueOf(reverseWord[count]);
        }

        JOptionPane.showMessageDialog(null, message);

    }

}
