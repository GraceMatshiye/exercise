import java.util.StringTokenizer;

/**
 * Created by Grace on 9/21/2015.
 */
public class The12TimesTables {

    public static void main(String[] args){

        int total = 0;
        String message = "Multiplication table up to 12 x 12 \n";

        for(int count1 = 1; count1 <= 12; count1++){

            for(int count2 = 1; count2 <= 12; count2++){
                total = count1 * count2;
                message = message + count1 + " x " + count2 + " = " + total + "\n";

            }
            message = message + "\n";
        }

        System.out.println(message);
    }
}
