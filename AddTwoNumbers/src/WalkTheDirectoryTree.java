import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
/**
 * Created by Grace on 9/29/2015.
 */
public class WalkTheDirectoryTree {

    public static void main(String[] args) throws IOException {
        Path start = FileSystems.getDefault().getPath("C:\\Users\\grace\\Documents\\DigitalGeekaship");



        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            public FileVisitResult visitFile(Path file,BasicFileAttributes attrs) throws IOException {
                if (file.toString().endsWith(".txt")) {
                    System.out.println(file);
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }
}
