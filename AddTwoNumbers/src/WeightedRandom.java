import java.util.Random;

/**
 * Created by Grace on 10/9/2015.
 */
public class WeightedRandom {
    public static void main(String args[]){
        String[] fruits = {"apples", "pears","grapes","oranges"};
        double[] weights = {0.25, 0.5, 0.2, 0.05};
        double[] accumulativeWeights = new double[weights.length];
        double accumulativeSum = 0;
        String selected = "";
        System.out.println("Displaying each fruit associated with its weight");
        for(int x = 0; x < weights.length ; x++){
            accumulativeSum += weights[x];
            accumulativeWeights[x] = accumulativeSum;
            System.out.print(fruits[x] + " => " + accumulativeWeights[x] + "\t");
        }
        System.out.println("\n" );
        Random rNumber = new Random();
        double number = Math.round(rNumber.nextDouble()*100)/100d ;
        System.out.print("The first randomly selected weight : " + number + " >> \"" );
        for(int count=0 ; count < accumulativeWeights.length ; count ++){
            if(accumulativeWeights[count] >= number ){
                selected = fruits[count];
                count = accumulativeWeights.length;
            }
        }
        System.out.print(selected + "\"");
    }
}
