import java.util.Scanner;

/**
 * Created by Grace on 9/23/2015.
 */
public class UserInput {

    public static void main(String[] args){

        Scanner userInput = new Scanner(System.in);

        System.out.println("Please enter a string : ");
        String aWord = userInput.nextLine();

        System.out.println("Please enter an integer number : ");
        int number = Integer.parseInt(userInput.nextLine());

        System.out.println("You entered '" + aWord  + "' which is string \nAnd the integer number you entered : " + number);

    }
}
