import javax.swing.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Grace on 10/8/2015.
 */
public class RunLengthEncoding {
    public static void main(String[] args) {
        String input = JOptionPane.showInputDialog(null, "Enter a string in order to encode it." +
                "\ne.g ABBB~CDDDDDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
        System.out.println("You entered ; \""+ input +"\"");
        System.out.println("Encode : " + encode(input));
        input = JOptionPane.showInputDialog(null, "Enter a string in order to decode it." +
                "\ne.g 1B12W3B24W");
        System.out.println("\nYou entered ; \""+ input +"\"");
        System.out.println("Decode : " + decode(input));
    }
    public static String encode(String source){
        StringBuffer stringSource = new StringBuffer();
        String code = "";
        for(int x = 0; x < source.length() ;x++){
            int length = 1;
            while(x+1 < source.length() && source.charAt(x) == source.charAt(x+1)){
                length++;
                x++;
            }
            stringSource.append(length);
            stringSource.append(source.charAt(x));
        }
        code = stringSource.toString();
        return code;
    }
    public static String decode(String source){
        StringBuffer stringSource = new StringBuffer();
        String translate = "";
        Pattern pattern = Pattern.compile("[0-9]+|[a-zA-Z]");
        Matcher match = pattern.matcher(source);
        while(match.find()){
            int count = Integer.parseInt(match.group());
            match.find();
            while(count-- != 0){
                stringSource.append(match.group());
            }
        }
        translate = stringSource.toString();
        return translate ;
    }
}
