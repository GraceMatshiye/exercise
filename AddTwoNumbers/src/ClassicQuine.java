/**
 * Created by Grace on 10/7/2015.
 */
public class ClassicQuine {
    public static void main(String[] args) {
         String sourceCode = "/**\n" +
                 " * Created by Grace on 10/7/2015.\n" +
                 " */\n" +
                 "public class ClassicQuine {\n" +
                 "    public static void main(String[] args) {\n" +
                 "         String sourceCode = \"/**\\n\" +\n" +
                 "                 \" * Created by Grace on 10/7/2015.\\n\" +\n" +
                 "                 \" */\\n\" +\n" +
                 "                 \"public class ClassicQuine {\\n\" +\n" +
                 "                 \"    public static void main(String[] args) {\\n\" +\n" +
                 "                 \"         String sourceCode = \\\"\\\";\\n\" +\n" +
                 "                 \"        System.out.println(sourceCode);\\n\" +\n" +
                 "                 \"    }\\n\" +\n" +
                 "                 \"}\";\n" +
                 "        System.out.println(sourceCode);\n" +
                 "    }\n" +
                 "}";
        System.out.println(sourceCode);
    }
}
