import org.omg.CORBA.PUBLIC_MEMBER;

/**
 * Created by Grace on 9/18/2015.
 */

import java.util.Random;
import java.util.Scanner;

public class callingMethods {

    public static void main(String[] args){

        //Calling a method that requires no arguments
        int firstNumber = getRandomNumber();
        System.out.println("First random number is : " + firstNumber);
        getEvenNumbers(firstNumber);    //Arguments are passed by value


        //Calling a method with a fixed number of arguments
        int fixedNumber = 100;
        getAddition(fixedNumber);   //Arguments are passed by value

        //Calling a method with optional arguments



        //Calling a method with a variable number of arguments
       Scanner input = new Scanner(System.in);

        System.out.println("Enter another number ");
        String strNumber = input.nextLine();
        int userNumber = Integer.parseInt(strNumber);

        System.out.println(getMultiply(userNumber));    //Arguments are passed by value


        //Obtaining the return value of a method
        System.out.println(getTwoRandomNumbers());

    }


    //A method that requires no arguments
    //A private method
    private static int getRandomNumber(){
        Random number = new Random();
        int intNumber = Math.abs(number.nextInt()) % 11;

        return(intNumber);
    }

    //A method with a fixed number of arguments
    //Public method
    public static void getAddition(int number){

        Scanner input = new Scanner(System.in);

        System.out.println("Enter a any number");
        String strNumber = input.nextLine();
        int userNumber = Integer.parseInt(strNumber) ;

        int sum = userNumber + number;
        System.out.println(sum);
    }

    //a method with a variable number of arguments
    //Public method
    public static int getMultiply(int number){
        Random randomNumber = new Random();
        int intNumber = Math.abs(randomNumber.nextInt()) % 11;

        int multiply = intNumber + number;
        return(multiply);
    }

    //A method returning a string value
    //Public method
    public static String getTwoRandomNumbers(){
        Random number = new Random();
        int number1 = Math.abs(number.nextInt()) % 11;
        int number2 = Math.abs(number.nextInt()) % 11;

        return("Two Random Numbers are: " + number1 + " and " + number2);
    }

    //Protected method
    protected static void getEvenNumbers(int number){
        int evenNumber = number % 2;
        if(evenNumber == 0){
            System.out.println(number + " is an even number.");
        }
        else {
            System.out.println(number + " is not an even number.");
        }
    }
}
